#include <stdio.h>
#include "bmp.h"
#include "image.h"
#include "file_handler.h"
#include "converter .h"
#include "image_rotator.h"
#include "util.h"

int main(int argc, char **argv) {
    if (argc < 2) print_error("ARGS_ERROR: Not enough arguments \n");
    if (argc > 2) print_error("ARGS_ERROR: Too many arguments \n");
    struct bmp_image image;
    if (read_bmp_image(argv[1], &image)) {
        struct image current_image = bmp_to_image(image);
        struct image inverted = rotate(&current_image, rotate90);
        struct bmp_image bmp_inv_image = image_to_bmp(inverted);

        if (write_bmp_image(argv[1], &bmp_inv_image)) {
            printf("Flip\n");
            discard_bmp_image(bmp_inv_image);
        }


    }
    return 0;
}
