//
// Created by bitway on 06.02.2021.
//

#ifndef BMP_FILE_HANDLER_H
#define BMP_FILE_HANDLER_H


#include <stdbool.h>
#include <bits/types/FILE.h>
#include <stddef.h>
#include <stdio.h>
#include "bmp.h"
bool read_bmp_header(FILE *f, struct bmp_header *header);

bool read_bmp_pixels(FILE *f, struct bmp_image *image);

bool read_bmp_image(const char *filename, struct bmp_image *image);

bool write_bmp_header(FILE *f, struct bmp_header *header);

bool write_bmp_pixels(FILE *f, struct bmp_image *image);

bool write_bmp_image(const char *filename, struct bmp_image *image);

#endif //BMP_FILE_HANDLER_H
