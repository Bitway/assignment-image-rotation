//
// Created by bitway on 06.02.2021.
//

#ifndef BMP_BMP_H
#define BMP_BMP_H

#include <inttypes.h>
#include <malloc.h>

struct __attribute__((packed)) bmp_header {

    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
struct bmp_pixel {
    uint8_t b, g, r;
};
struct bmp_image {
    struct bmp_header header;
    struct bmp_pixel *pixels;
};

void discard_bmp_image(struct bmp_image image);
#endif //BMP_BMP_H
