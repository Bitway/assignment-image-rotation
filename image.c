//
// Created by bitway on 06.02.2021.
//

#include <string.h>
#include <stdlib.h>
#include "image.h"


struct image image_create(uint32_t width, uint32_t height) {
    struct image image;
    image.width = width;
    image.height = height;
    image.pixels = malloc(sizeof(struct pixel) * width * height);
    return image;
}

void image_discard(struct image image) {
    free(image.pixels);
}

