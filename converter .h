//
// Created by bitway on 07.02.2021.
//
#include "bmp.h"
#include "image.h"
#ifndef BMP_CONVERTER_H
#define BMP_CONVERTER_H

struct image bmp_to_image(struct bmp_image bmpImage);
struct bmp_image image_to_bmp(struct image image);

#endif //BMP_CONVERTER_H
