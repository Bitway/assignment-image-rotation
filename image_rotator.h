//
// Created by bitway on 07.02.2021.
//
#include "image.h"

#ifndef BMP_IMAGE_ROTATOR_H
#define BMP_IMAGE_ROTATOR_H

typedef struct image rotator(struct image *);

struct image rotate(struct image *image, rotator r);

struct image rotate90(struct image *image);


#endif //BMP_IMAGE_ROTATOR_H
