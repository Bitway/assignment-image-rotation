//
// Created by bitway on 06.02.2021.
//
#include <stdint.h>
#include <stdbool.h>

#ifndef BMP_IMAGE_H
#define BMP_IMAGE_H
struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};
struct image {
    uint32_t width;
    uint32_t height;

    struct pixel *pixels;
};

struct image image_create(uint32_t width, uint32_t height);

void image_discard(struct image image);

#endif //BMP_IMAGE_H
