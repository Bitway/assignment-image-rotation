CFLAGS=--std=c18 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc

all: image_rotate

bmp.o: bmp.c
	$(CC) -c $(CFLAGS) $< -o $@

util.o: util.c
	$(CC) -c $(CFLAGS) $< -o $@

image.o: image.c
	$(CC) -c $(CFLAGS) $< -o $@

converter.o: converter.c
	$(CC) -c $(CFLAGS) $< -o $@

file_handler.o: file_handler.c
	$(CC) -c $(CFLAGS) $< -o $@

image_rotator.o: image_rotator.c
	$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c
	$(CC) -c $(CFLAGS) $< -o $@

image_rotate: main.o util.o bmp.o image.o converter.o file_handler.o image_rotator.o
	$(CC) -o image_rotate $^

clean:
	rm -f main.o util.o bmp.o converter.o file_handler.o image.o image_rotator.o  image_rotate

