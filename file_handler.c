//
// Created by bitway on 06.02.2021.
//
#include <stdio.h>
#include <malloc.h>
#include "file_handler.h"
#include "util.h"


bool read_bmp_header(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

bool read_bmp_pixels(FILE *f, struct bmp_image *image) {
    size_t size = image->header.biWidth * image->header.biHeight;
    image->pixels = malloc(sizeof(struct bmp_pixel) * size);
    int32_t rowOffset = image->header.biWidth % 4;
    for (int32_t row = image->header.biHeight - 1; row >= 0; --row) {
        int32_t read_count = fread(image->pixels + row * image->header.biWidth, sizeof(struct bmp_pixel),
                                   image->header.biWidth, f);
        if ((uint32_t) read_count < image->header.biWidth) {
            free(image->pixels);
            print_error("READ_ERROR: Invalid bitmap\n");
            return false;
        }

        if (fseek(f, rowOffset, SEEK_CUR)) {
            print_error("READ_ERROR: Incorrect rowOffset\n");
            free(image->pixels);
            return false;
        }
    }
    return true;

}

bool read_bmp_image(const char *filename, struct bmp_image *image) {
    if (!filename) return false;
    FILE *f = fopen(filename, "rb");
    if (!f) {
        print_error("READ_ERROR: File does not open\n");
        return false;
    }

    if (read_bmp_header(f, &image->header)) {
        if (read_bmp_pixels(f, image)) {
            fclose(f);
            return true;
        }

        fclose(f);
        return false;
    }
    print_error("READ_ERROR: Invalid header\n");
    fclose(f);
    return false;
}

bool write_bmp_header(FILE *f, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, f);
}

bool write_bmp_pixels(FILE *f, struct bmp_image *image) {
    int32_t row, row_offset;
    static uint8_t buffer[] = {0};
    row_offset = image->header.biWidth % 4;
    for (row = image->header.biHeight - 1; row >= 0; --row) {
        if (fwrite(image->pixels + row * image->header.biWidth,
                   sizeof(struct bmp_pixel), image->header.biWidth, f) < image->header.biWidth) {
            print_error("WRITE_ERROR: Invalid bitmap\n");
            return false;
        }

        if ((int32_t) fwrite(buffer, 1, row_offset, f) < row_offset) {
            print_error("WRITE_ERROR: Invalid row_offset\n");
            return false;
        }
    }
    return true;
}

bool write_bmp_image(const char *filename, struct bmp_image *image) {
    if (!filename) {
        print_error("WRITE_ERROR: File does not open\n");
        return false;
    }
    FILE *f = fopen(filename, "wb");
    if (write_bmp_header(f, &image->header)) {
        if (write_bmp_pixels(f, image)) {
            return true;
        }
    }
    print_error("WRITE_ERROR: Invalid header\n");
    return false;
}

