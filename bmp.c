//
// Created by bitway on 07.02.2021.
//

#include "bmp.h"

void discard_bmp_image(struct bmp_image image) {
    free(image.pixels);
}

