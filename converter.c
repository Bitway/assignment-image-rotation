//
// Created by bitway on 07.02.2021.
//

#include <stddef.h>
#include "image.h"
#include "bmp.h"

struct image bmp_to_image(struct bmp_image bmpImage) {
    struct image image = image_create(bmpImage.header.biWidth, bmpImage.header.biHeight);
    for (size_t i = 0; i < bmpImage.header.biWidth * bmpImage.header.biHeight; ++i) {
        image.pixels[i].r = bmpImage.pixels[i].r;
        image.pixels[i].g = bmpImage.pixels[i].g;
        image.pixels[i].b = bmpImage.pixels[i].b;
    }
    discard_bmp_image(bmpImage);
    return image;
}

struct bmp_image image_to_bmp(struct image image) {
    struct bmp_image bmp_img;
    bmp_img.header.bfType = 19778;
    bmp_img.header.bOffBits = sizeof(struct bmp_header);
    bmp_img.header.biWidth = image.width;
    bmp_img.header.biHeight = image.height;
    bmp_img.header.biSize = 40;
    bmp_img.header.biPlanes = 1;
    bmp_img.header.biBitCount = 24;
    bmp_img.header.biCompression = 0;
    bmp_img.header.biSizeImage =
            bmp_img.header.biHeight * (bmp_img.header.biWidth * sizeof(struct bmp_pixel) + bmp_img.header.biWidth % 4);
    bmp_img.header.bfileSize = bmp_img.header.bOffBits + bmp_img.header.biSizeImage;
    size_t size = bmp_img.header.biHeight * bmp_img.header.biWidth;
    bmp_img.pixels = malloc(sizeof(struct bmp_pixel) * size);
    for (size_t i = 0; i < size; ++i) {
        bmp_img.pixels[i].r = image.pixels[i].r;
        bmp_img.pixels[i].g = image.pixels[i].g;
        bmp_img.pixels[i].b = image.pixels[i].b;
    }
    image_discard(image);
    return bmp_img;
}