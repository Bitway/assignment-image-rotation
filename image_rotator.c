//
// Created by bitway on 07.02.2021.
//
#include <stddef.h>
#include "image_rotator.h"


struct image rotate(struct image *image, rotator r) {
    return r(image);
}

struct image rotate90(struct image *image) {
    struct image inverted = image_create(image->height, image->width);
    uint32_t line_start = (image->height - 1) * image->width;
    uint32_t size = image->width * image->height;
    uint32_t row = 0;
    uint32_t i = 0;
    while (i < size) {

        for (uint32_t j = 0; j < image->height; ++j) {
            inverted.pixels[i].r = image->pixels[line_start - row].r;
            inverted.pixels[i].g = image->pixels[line_start - row].g;
            inverted.pixels[i].b = image->pixels[line_start - row].b;
            row += image->width;
            i++;

        }
        line_start++;
        row = 0;

    }
    image_discard(*image);
    return inverted;
}